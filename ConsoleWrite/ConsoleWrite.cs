﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;
using System.Collections.Generic;
using System.Text;

namespace ConsoleWriteEngine
{
    #region Structs
    [StructLayout(LayoutKind.Sequential)]
    public struct Coord
    {
        public short X;
        public short Y;

        public Coord(short X, short Y)
        {
            this.X = X;
            this.Y = Y;
        }
    };

    [StructLayout(LayoutKind.Explicit)]
    public struct CharUnion
    {
        [FieldOffset(0)]
        public char UnicodeChar;
        [FieldOffset(0)]
        public byte AsciiChar;
    }

    [StructLayout(LayoutKind.Explicit)]
    public struct CharInfo
    {
        [FieldOffset(0)]
        public CharUnion Char;
        [FieldOffset(2)]
        public short Attributes;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SmallRect
    {
        public short Left;
        public short Top;
        public short Right;
        public short Bottom;
    }
    #endregion

    public class ConsoleWrite
    {
        #region DLL Imports
        [DllImport("Kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern SafeFileHandle CreateFile(
            string fileName,
            [MarshalAs(UnmanagedType.U4)] uint fileAccess,
            [MarshalAs(UnmanagedType.U4)] uint fileShare,
            IntPtr securityAttributes,
            [MarshalAs(UnmanagedType.U4)] FileMode creationDisposition,
            [MarshalAs(UnmanagedType.U4)] int flags,
            IntPtr template);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool WriteConsoleOutput(
          SafeFileHandle hConsoleOutput,
          CharInfo[] lpBuffer,
          Coord dwBufferSize,
          Coord dwBufferCoord,
          ref SmallRect lpWriteRegion);
        #endregion

        private int BufferWidth = 80;
        private int BufferHeight = 25;

        public ConsoleWrite(int BufferWidth, int BufferHeight)
        {
            this.BufferWidth = BufferWidth;
            this.BufferHeight = BufferHeight;
        }

        public ConsoleWrite()
        {
            this.BufferWidth = 80;
            this.BufferHeight = 25;
        }

        public void Write(int X, int Y, string Text, 
            ConsoleColor ForegroundColor = ConsoleColor.Gray, 
            ConsoleColor BackgroundColor = ConsoleColor.Black, bool wordwrap = true)
        {
            SafeFileHandle h = CreateFile("CONOUT$", 0x40000000, 2, IntPtr.Zero, FileMode.Open, 0, IntPtr.Zero);
            
            if (!h.IsInvalid)
            {
                CharInfo[] buf = new CharInfo[this.BufferWidth * this.BufferHeight ];
                SmallRect rect = new SmallRect() { Left = (short)X, Top = (short)Y,
                    Right = (short)(this.BufferWidth + X), Bottom = (short)(this.BufferHeight + Y) };

                if (wordwrap)
                    Text = WordWrap(Text, this.BufferWidth);

                if (Text.Length > buf.Length) Text = Text.Substring(0, buf.Length - 1);

                int x = 0, y = 0;
                for (int i = 0; i < Text.Length; ++i)
                {
                    switch (Text[i])
                    {
                        case '\n': // newline char, move to next line, aka y=y+1
                            y++;
                            break;
                        case '\r': // carriage return, aka back to start of line
                            x = 0;
                            break;
                        default:
                            int j = y * this.BufferWidth + x;
                            if (j < buf.Length)
                            {
                                buf[j].Attributes = (short)((short)ForegroundColor | (short)((short)BackgroundColor * 0x0010));
                                buf[j].Char.AsciiChar = (byte)Text[i];
                                x++;
                            }
                            break;
                    }

                }

                bool b = WriteConsoleOutput(h, buf,
                    new Coord() { X = (short)(this.BufferWidth), Y = (short)(this.BufferHeight) },
                    new Coord() { X = 0, Y = 0 },
                    ref rect);
            }
        }

        public void Write(int X, int Y, CharInfo[] @CharInfo)
        {
            SafeFileHandle h = CreateFile("CONOUT$", 0x40000000, 2, IntPtr.Zero, FileMode.Open, 0, IntPtr.Zero);

            if (!h.IsInvalid)
            {
                SmallRect rect = new SmallRect() { Left = (short)X, Top = (short)Y, Right = (short)(this.BufferWidth + X), Bottom = (short)(this.BufferHeight + Y) };

                bool b = WriteConsoleOutput(h, CharInfo,
                    new Coord() { X = (short)(this.BufferWidth), Y = (short)(this.BufferHeight) },
                    new Coord() { X = 0,  Y = 0 },
                    ref rect);
            }
        }

        public void SetBufferSize(int Width, int Height)
        {
            this.BufferWidth = Width;
            this.BufferHeight = Height;
        }

        #region utility methods
        static char[] splitChars = new char[] { ' ', '-', '\t' };

        public static string WordWrap(string str, int width)
        {
            string[] words = Explode(str, splitChars);

            int curLineLength = 0;
            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < words.Length; i += 1)
            {
                string word = words[i];
                // If adding the new word to the current line would be too long,
                // then put it on a new line (and split it up if it's too long).
                if (curLineLength + word.Length > width)
                {
                    // Only move down to a new line if we have text on the current line.
                    // Avoids situation where wrapped whitespace causes emptylines in text.
                    if (curLineLength > 0)
                    {
                        strBuilder.Append(Environment.NewLine);
                        curLineLength = 0;
                    }

                    // If the current word is too long to fit on a line even on it's own then
                    // split the word up.
                    while (word.Length > width)
                    {
                        strBuilder.Append(word.Substring(0, width - 1) + "-");
                        word = word.Substring(width - 1);

                        strBuilder.Append(Environment.NewLine);
                    }

                    // Remove leading whitespace from the word so the new line starts flush to the left.
                    word = word.TrimStart();
                }
                strBuilder.Append(word);
                curLineLength += word.Length;
            }

            return strBuilder.ToString();
        }

        private static string[] Explode(string str, char[] splitChars)
        {
            List<string> parts = new List<string>();
            int startIndex = 0;
            while (true)
            {
                int index = str.IndexOfAny(splitChars, startIndex);

                if (index == -1)
                {
                    parts.Add(str.Substring(startIndex));
                    return parts.ToArray();
                }

                string word = str.Substring(startIndex, index - startIndex);
                char nextChar = str.Substring(index, 1)[0];
                // Dashes and the likes should stick to the word occuring before it. Whitespace doesn't have to.
                if (char.IsWhiteSpace(nextChar))
                {
                    parts.Add(word);
                    parts.Add(nextChar.ToString());
                }
                else
                {
                    parts.Add(word + nextChar);
                }

                startIndex = index + 1;
            }
        }
        #endregion
    }
}