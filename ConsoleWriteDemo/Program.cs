﻿using System;
using System.Timers;

using ConsoleWriteEngine;

namespace ConsoleWriteDemo
{
    class Program
    {
        public static string CONSOLE_TITLE = "ConsoleWriteDemo";
        public static int CONSOLE_WIDTH = 80;
        public static int CONSOLE_HEIGHT = 25;

        // Create an instance of ConsoleWrite
        ConsoleWrite writer = new ConsoleWrite();

        static Timer wereDone;
        int wereDoneCounter = 0;

        public Program()
        {
            // Set the buffer's current width and height
            writer.SetBufferSize(Program.CONSOLE_WIDTH - 2, Program.CONSOLE_HEIGHT - 2);
            
            // Create CharInfo buffer
            CharInfo[] charBuffer = new CharInfo[(Program.CONSOLE_WIDTH - 2) * (Program.CONSOLE_HEIGHT - 2)];
            
            // Go through the letters of the alphabet
            for (int j = 0; j < 26; j++)
            {
                // Go through each element of the charBuffer array
                for (int i = 0; i < charBuffer.Length; i++)
                {
                    // Assign a letter to the array
                    charBuffer[i].Char.AsciiChar = (byte)(65 + j);

                    // Assign color attributes to array
                    charBuffer[i].Attributes = ((short)ConsoleColor.Red | ((short)ConsoleColor.Blue * 0x0010));
                }

                // Write current buffer to the screen
                writer.Write(1, 1, charBuffer);

                // Delay the buffer write so we can see it write
                //System.Threading.Thread.Sleep(10);
            }

            // Write a single lines centered in the console window
            string textToWrite = "WE JUST FINISHED OVERWRITING THE CONSOLE SUPER FAST!!";
            writer.SetBufferSize(textToWrite.Length, 1);
            writer.Write((Program.CONSOLE_WIDTH / 2) - (textToWrite.Length / 2), 
                (Program.CONSOLE_HEIGHT / 2) - 1, textToWrite, ConsoleColor.Red);
            writer.Write((Program.CONSOLE_WIDTH / 2) - (textToWrite.Length / 2),
                (Program.CONSOLE_HEIGHT / 2), textToWrite, ConsoleColor.White);
            writer.Write((Program.CONSOLE_WIDTH / 2) - (textToWrite.Length / 2),
                (Program.CONSOLE_HEIGHT / 2) + 1, textToWrite, ConsoleColor.Blue);

            // Timer done message
            wereDone = new Timer(100);
            wereDone.Elapsed += new ElapsedEventHandler(wereDone_Elapsed);
            wereDone.Start();

            // Stop the console from closing
            Console.ReadKey(true);
        }

        void wereDone_Elapsed(object obj, ElapsedEventArgs e)
        {
            string done = "We're done!";
            ConsoleColor[] colorList = new ConsoleColor[]
            {
                ConsoleColor.Red, ConsoleColor.Blue, ConsoleColor.White
            };

            writer.SetBufferSize(1, 1);
            writer.Write((Program.CONSOLE_WIDTH / 2) - (done.Length / 2) + wereDoneCounter,
                (Program.CONSOLE_HEIGHT / 2) + 5, done[wereDoneCounter].ToString(), 
                colorList[new Random().Next(0, colorList.Length)]);

            wereDoneCounter++;

            if (wereDoneCounter > done.Length - 1)
                wereDone.Stop();
        }

        static void Main(string[] args)
        {
            ConfigureSettings();

            Program prog = new Program();
        }

        static void ConfigureSettings()
        {
            Console.Title = CONSOLE_TITLE;
            Console.WindowWidth = CONSOLE_WIDTH;
            Console.BufferWidth = Console.WindowWidth;
            Console.WindowHeight = CONSOLE_HEIGHT;
            Console.BufferHeight = Console.WindowHeight;

            Console.CursorVisible = false;
        }
    }
}
