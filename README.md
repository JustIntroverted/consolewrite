﻿# Hello all, #

I created this project so I could write quicker to the C# console window in an attempt to make a roguelike.  What I've actually done is borrowed posted code from user, [Chris Taylor](http://stackoverflow.com/users/314028/chris-taylor), of [Stack Overflow](http://stackoverflow.com) on this page here: [http://stackoverflow.com/questions/2754518/how-can-i-write-fast-colored-output-to-console](http://stackoverflow.com/questions/2754518/how-can-i-write-fast-colored-output-to-console).

That said, I've simplified the process a little more, and made a few methods to output code to the console.

If you'd like to look at it, feel free to.  I've added a demo project with it, so you can test it out without knowing anything about it.  The comments I added should be *somewhat* helpful.  :3